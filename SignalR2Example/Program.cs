﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.IO;

namespace SignalR2Example
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())    //content files eg. Views. To work views need to add <PreserveCompilationContext>true</PreserveCompilationContext> in csproj
                .UseStartup<Startup>()                              //invoke Startup class
                .Build();

            host.Run();
        }
    }
}
