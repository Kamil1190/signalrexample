﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalR2Example.Hubs
{
    public class Chat : Hub
    {
        public Task Send(string message)
        {
            return Clients.All.InvokeAsync("Send", message);
        }

        public Task ChangeStatus(string value)
        {
            return Clients.All.InvokeAsync("ChangeStatus", value);
        }
    }
}
