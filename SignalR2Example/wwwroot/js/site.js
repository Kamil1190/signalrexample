﻿let connection = new signalR.HubConnection('/chat');

let messagesContainer = $("#messages-container"),
    sendButton = $("#send-button"),
    messageInput = $("#message-input"),
    chatRadios = $('input[type=radio][name=status]'),
        enableRadio = $('input[type=radio][name=status][value="enabled"]'),
        disableRadio = $('input[type=radio][name=status][value="disabled"]');

sendButton.on("click", sendFunction);
chatRadios.on("change", changeStatusFunction);

connection.on('send', data => {
    messagesContainer.append('<div class="message"><p>Message:</p><p>' + data + '</p></div>');
});

connection.on('changeStatus', data => {
    if (data === "enabled") {
        enableRadio.prop('checked', true);
        disableRadio.prop('checked', false);
        messageInput.prop('disabled', false);
    }
    else
    {
        enableRadio.prop('checked', false);
        disableRadio.prop('checked', true);
        messageInput.prop('disabled', true);
    }
});

connection.start()
    .then(() => connection.invoke('send', 'New client'));

function sendFunction() {
    connection.invoke('send', messageInput.val());
    messageInput.val("");
}

function changeStatusFunction() {
    connection.invoke('changeStatus', this.value);
}